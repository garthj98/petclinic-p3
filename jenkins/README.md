# Steps to get Jenkins to work 

### <u>Pre requisities</u>
- AMI for Jenkins Master in AWS - Region: Ireland
- AMI for Jenkins Agent node in AWS - Region: Ireland
- SSH key saved locally

-----------------------------------------------------------------------------------------------------
### <u>Background</u>

Jenkins Master and Agent node have both been spun up from an AMI in Ireland and deployed using Terraform. 

- **Master = ami-0fd352ce5a8aaece5** 
- **Agent  = ami-011e4185285f941f5**

Should you destroy/re-deploy the two Jenkins machines using the Terraform script, you will need to follow the following steps to get them back to working state.

### Step 1: SSH into Jenkins master

```bash
$ ssh -i ~/.ssh/<nameofkey> ubuntu@<PublicIP>
```

### Step 2: Switch to Jenkin's user.

```bash
$ su - jenkins
# The password for Jenkins is *gans-jenkins*
```

### Step 3: SSH into Agent node user Jenkins

```bash
$ ssh jenkins@<PrivateIPnode>
# The SSH keypair is already generated and stored
```

### Step 4: Relaunch Agent node

You will need to navigate to *http://gans-jenkins.academy.labs.automationlogic.com:8080/* and log into Jenkins with:

- **Username:** *gans*
- **Password:** *cohort-9*

Once logged in, click on **Manage Jenkins** on the left hand side and then **Manage Nodes and Clouds** and configure **Jenkins-Agent-Node**. Under launch method, paste the new private IP of the Agent Node and the click **Relaunch Agent Node**. 

You are now all set to configure the CI/CD pipelines to run tests and deploy new AMIs.




### Who to contact

To gain access to SSH key and password for Jenkins user on Agent Node, discuss any issues with setting Jenkins back up, or just have some feedback on our code, you can contact **Shannon** at: shannon.tamplin@automationlogic.com 

Our working hours are Monday to Friday 9am - 5:30pm 





