terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = var.aws_region
}

resource "aws_vpc" "my-vpc" {
    cidr_block = var.vpc_cidr
    tags = {
        Name = "${var.my_tag}-vpc"
    }
}

resource "aws_internet_gateway" "my-internet-gateway" {
    vpc_id = aws_vpc.my-vpc.id
    tags = {
        Name = "${var.my_tag}_IGW"
    }
}

resource "aws_network_acl" "my-public-nacl" {
  vpc_id = aws_vpc.my-vpc.id
  subnet_ids = [aws_subnet.public-subnet.id, aws_subnet.public-subnet-2.id]

  egress {
      protocol   = -1
      rule_no    = 100
      action     = "allow"
      cidr_block = "0.0.0.0/0"
      from_port  = 0
      to_port    = 0
    }

  ingress {
      protocol   = -1
      rule_no    = 100
      action     = "allow"
      cidr_block = "0.0.0.0/0"
      from_port  = 0
      to_port    = 0
  }

  tags = {
    Name = "${var.my_tag}_public_NACL"
  }
}

resource "aws_network_acl" "my-private-nacl" {
  vpc_id = aws_vpc.my-vpc.id
  subnet_ids = [aws_subnet.private-subnet.id]

  egress {
      protocol   = -1
      rule_no    = 100
      action     = "allow"
      cidr_block = var.vpc_cidr
      from_port  = 0
      to_port    = 0
    }

  ingress {
      protocol   = -1
      rule_no    = 100
      action     = "allow"
      cidr_block = var.vpc_cidr
      from_port  = 0
      to_port    = 0
  }

  tags = {
    Name = "${var.my_tag}_private_NACL"
  }
}

/*
  Public Subnet
*/

resource "aws_subnet" "public-subnet" {
    vpc_id = aws_vpc.my-vpc.id

    cidr_block = var.public_subnet_cidr
    availability_zone = "${var.aws_region}a"
    map_public_ip_on_launch = true

    tags = {
        Name = "${var.my_tag}_Public_Subnet"
    }
}

resource "aws_subnet" "public-subnet-2" {
    vpc_id = aws_vpc.my-vpc.id

    cidr_block = var.public_subnet_cidr_2
    availability_zone = "${var.aws_region}b"
    map_public_ip_on_launch = true

    tags = {
        Name = "${var.my_tag}_Public_Subnet_2"
    }
}

resource "aws_route_table" "my-route-table" {
    vpc_id = aws_vpc.my-vpc.id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.my-internet-gateway.id
    }

    tags = {
        Name = "${var.my_tag}_Route_Table_public"
    }
}

# Creating an association between a route table and a subnet or a route table and an internet gateway or virtual private gateway
resource "aws_route_table_association" "my-route-table-association" {
    subnet_id = aws_subnet.public-subnet.id
    route_table_id = aws_route_table.my-route-table.id
}

resource "aws_route_table_association" "my-route-table-association-2" {
    subnet_id = aws_subnet.public-subnet-2.id
    route_table_id = aws_route_table.my-route-table.id
}

/*
Private Subnet 
*/

resource "aws_subnet" "private-subnet" {
  vpc_id     = aws_vpc.my-vpc.id
  cidr_block = var.private_subnet_cidr
  availability_zone = "${var.aws_region}a"

  tags = {
    Name = "${var.my_tag}-private-subnet"
  }
}

resource "aws_eip" "nat_gw_eip" {
  vpc = true
}

resource "aws_nat_gateway" "gw" {
  allocation_id = aws_eip.nat_gw_eip.id
  subnet_id     = aws_subnet.public-subnet.id
}

resource "aws_route_table" "my-private-route-table" {
    vpc_id = aws_vpc.my-vpc.id

    route {
        cidr_block = "0.0.0.0/0"
        nat_gateway_id = aws_nat_gateway.gw.id
    }

    tags = {
        Name = "Main Route Table for NAT-ed subnet"
    }
}

resource "aws_route_table_association" "my_route_table_association_private" {
    subnet_id = aws_subnet.private-subnet.id
    route_table_id = aws_route_table.my-private-route-table.id
}

