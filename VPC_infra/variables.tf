variable "my_tag" {
	description = "Name to tag all elements with"
	default = "GANS"
}

variable "aws_region" {
    description = "AWS region"
    default = "eu-west-1"
}

variable "vpc_cidr" {
    description = "CIDR for the whole VPC"
    default = "10.0.0.0/16"
}

variable "public_subnet_cidr" {
    description = "CIDR for the public subnet"
    default = "10.0.1.0/24"    
}

variable "public_subnet_cidr_2" {
    description = "CIDR for the public subnet 2"
    default = "10.0.2.0/24"    
}

variable "private_subnet_cidr" {
    description = "CIDR for the private subnet "
    default = "10.0.3.0/24"    
}
