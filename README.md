# Petclinic Assessment 3

## <u>Description</u>

This repository has our terraform script which creates the environment for Petclinic. Petclinic is also connected to an RDS database. We have used Jenkins for our CI/CD pipelines which have been configured to create new AMIs using packer and deploying them using terraform plan.  

If the environment needs to be set up please see the instuctions stated in https://bitbucket.org/garthj98/petclinic-p3/src/master/terraform_main/README.md.

If the Jenkins environment has aleady been formed updates can be tested and built in just two simple commands initially:

```bash
# Clone repository to your local machine
$ git clone git@bitbucket.org:garthj98/petclinic-p3.git

# Make a change to the petclinic branch and then push to petclinic branch
$ git push origin petclinic
```
--------------------------------------------------------------------------------------

### **Petclinic**

[click here](https://bitbucket.org/garthj98/petclinic-p3/src/master/petclinic_launch/README.md) to navigate to the Petclinic README.md


### **RDS**

[click here](https://bitbucket.org/garthj98/petclinic-p3/src/master/RDS/RDS-README.md) to navigate to the RDS README.md

### **Terraform**

[click here](https://bitbucket.org/garthj98/petclinic-p3/src/master/terraform_main/README.md) to navigate to the Terraform README.md

### **Jenkins**

[click here](https://bitbucket.org/garthj98/petclinic-p3/src/master/jenkins/README.md)
to navigate to the Jenkins README.md



