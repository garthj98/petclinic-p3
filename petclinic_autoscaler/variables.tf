variable "my_tag" {
	description = "Name to tag all elements with"
	default = "gans"
}

variable "vpc_id" {
    description = "VPC ID"
    default = "vpc-4bb64132"
}