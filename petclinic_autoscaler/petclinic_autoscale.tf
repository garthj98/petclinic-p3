terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "eu-west-1" 
}

resource "aws_lb" "main_lb" {
    name                        = "${var.my_tag}-lb"
    internal                    = false
    load_balancer_type          = "application"
    # need to edit for our security groups
    security_groups             = ["sg-026a52fbbe4783f73", "sg-097bc6de9c56aad49"]
    # need to edit for our VPC 
    subnets                     = ["subnet-953a58cf", "subnet-7bddfc1d", "subnet-a94474e1"]
    tags                        = {
        Name                    = "${var.my_tag}-lb"
    }
}

resource "aws_lb_target_group" "my_tg" {
  name     = "${var.my_tag}-lb-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = var.vpc_id
}

resource "aws_lb_listener" "tg_listener" {
  load_balancer_arn = aws_lb.main_lb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.my_tg.arn
  }
}

resource "aws_launch_template" "my-template" {
  name          = "${var.my_tag}-template"
  # need to edit for our image
  image_id      = "ami-0d6ad9c264f91e96f"
  instance_type = "t2.micro"
  # need to edit for our VPC 
  vpc_security_group_ids = ["sg-026a52fbbe4783f73", "sg-097bc6de9c56aad49", "sg-072a803b313e9637f"]
  key_name = "Cohort9_shared"
  tags = {
    Name = "${var.my_tag}-tf"
  }
  # iam_instance_profile {
  #   name = "${var.my_tag}_test"
  # }
  user_data = filebase64("${path.module}/userdata.sh")
}

resource "aws_autoscaling_group" "my-asg" {
  desired_capacity   = 1
  max_size           = 3
  min_size           = 1
  # need to edit for our VPC 
  vpc_zone_identifier = ["subnet-953a58cf", "subnet-7bddfc1d", "subnet-a94474e1"]
  launch_template {
    id      = aws_launch_template.my-template.id
    version = "$Latest"
  }
}

resource "aws_autoscaling_attachment" "asg_attachment_bar" {
  autoscaling_group_name = aws_autoscaling_group.my-asg.id
  alb_target_group_arn    = aws_lb_target_group.my_tg.arn
}