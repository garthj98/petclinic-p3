#! /bin/bash

sleep 180

pass="petclinic"

mysql -h ${hostname} -P 3306 -u petclinic -p$pass < /home/ec2-user/spring-petclinic/src/main/resources/db/mysql/user.sql
mysql -h ${hostname} -P 3306 -u petclinic -p$pass petclinic < /home/ec2-user/spring-petclinic/src/main/resources/db/mysql/schema.sql
mysql -h ${hostname} -P 3306 -u petclinic -p$pass petclinic < /home/ec2-user/spring-petclinic/src/main/resources/db/mysql/data.sql

