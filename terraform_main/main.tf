terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = var.aws_region
}

terraform {
  backend "s3" {
    encrypt = true
    bucket = "gans-bucket" 
    key    = "infra.tfstate-academy"
    region = "eu-west-1"
  }
}

resource "aws_vpc" "my-vpc" {
    cidr_block = var.vpc_cidr
    enable_dns_hostnames = true
    tags = {
        Name = "${var.my_tag}-vpc"
    }
}

resource "aws_internet_gateway" "my-internet-gateway" {
    vpc_id = aws_vpc.my-vpc.id
    tags = {
        Name = "${var.my_tag}_IGW"
    }
}

resource "aws_network_acl" "my-public-nacl" {
  vpc_id = aws_vpc.my-vpc.id
  subnet_ids = [aws_subnet.public-subnet.id, aws_subnet.public-subnet-2.id]

  egress {
      protocol   = -1
      rule_no    = 100
      action     = "allow"
      cidr_block = "0.0.0.0/0"
      from_port  = 0
      to_port    = 0
    }

  ingress {
      protocol   = -1
      rule_no    = 100
      action     = "allow"
      cidr_block = "0.0.0.0/0"
      from_port  = 0
      to_port    = 0
  }

  tags = {
    Name = "${var.my_tag}-public-NACL"
  }
}

resource "aws_network_acl" "my-private-nacl" {
  vpc_id = aws_vpc.my-vpc.id
  subnet_ids = [aws_subnet.private-subnet.id]

  egress {
      protocol   = -1
      rule_no    = 100
      action     = "allow"
      cidr_block = var.vpc_cidr
      from_port  = 0
      to_port    = 0
    }

  ingress {
      protocol   = -1
      rule_no    = 100
      action     = "allow"
      cidr_block = var.vpc_cidr
      from_port  = 0
      to_port    = 0
  }

  tags = {
    Name = "${var.my_tag}-private-NACL"
  }
}

/*
  Public Subnet
*/

resource "aws_subnet" "public-subnet" {
    vpc_id = aws_vpc.my-vpc.id

    cidr_block = var.public_subnet_cidr
    availability_zone = "${var.aws_region}a"
    map_public_ip_on_launch = true

    tags = {
        Name = "${var.my_tag}-Public-Subnet"
    }
}

resource "aws_subnet" "public-subnet-2" {
    vpc_id = aws_vpc.my-vpc.id

    cidr_block = var.public_subnet_cidr_2
    availability_zone = "${var.aws_region}b"
    map_public_ip_on_launch = true

    tags = {
        Name = "${var.my_tag}-Public-Subnet_2"
    }
}

resource "aws_route_table" "my-route-table" {
    vpc_id = aws_vpc.my-vpc.id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.my-internet-gateway.id
    }

    tags = {
        Name = "${var.my_tag}-Route-Table-Public"
    }
}

# Creating an association between a route table and a subnet or a route table and an internet gateway or virtual private gateway
resource "aws_route_table_association" "my-route-table-association" {
    subnet_id = aws_subnet.public-subnet.id
    route_table_id = aws_route_table.my-route-table.id
}

resource "aws_route_table_association" "my-route-table-association-2" {
    subnet_id = aws_subnet.public-subnet-2.id
    route_table_id = aws_route_table.my-route-table.id
}

/*
Private Subnets
*/

resource "aws_subnet" "private-subnet" {
  vpc_id     = aws_vpc.my-vpc.id
  cidr_block = var.private_subnet_cidr
  availability_zone = "${var.aws_region}a"

  tags = {
    Name = "${var.my_tag}-private-subnet"
  }
}

resource "aws_subnet" "private-subnet-2" {
  vpc_id     = aws_vpc.my-vpc.id
  cidr_block = var.private_subnet_cidr_2
  availability_zone = "${var.aws_region}b"

  tags = {
    Name = "${var.my_tag}-private-subnet-2"
  }
}

resource "aws_eip" "nat-gw-eip" {
  vpc = true
}

resource "aws_eip" "nat-gw-eip2" {
  vpc = true
}

resource "aws_nat_gateway" "gw" {
  allocation_id = aws_eip.nat-gw-eip.id
  subnet_id     = aws_subnet.public-subnet.id
}

resource "aws_nat_gateway" "gw2" {
  allocation_id = aws_eip.nat-gw-eip2.id
  subnet_id     = aws_subnet.public-subnet-2.id
}

resource "aws_route_table" "my-private-route-table" {
    vpc_id = aws_vpc.my-vpc.id

    route {
        cidr_block = "0.0.0.0/0"
        nat_gateway_id = aws_nat_gateway.gw.id
    }

    tags = {
        Name = "${var.my_tag}-Route-Table-private"
    }
}

resource "aws_route_table" "my-private-route-table-2" {
    vpc_id = aws_vpc.my-vpc.id

    route {
        cidr_block = "0.0.0.0/0"
        nat_gateway_id = aws_nat_gateway.gw2.id
    }

    tags = {
        Name = "${var.my_tag}-Route-Table-private-2"
    }
}

resource "aws_route_table_association" "my-route-table-association-private" {
    subnet_id = aws_subnet.private-subnet.id
    route_table_id = aws_route_table.my-private-route-table.id
}

resource "aws_route_table_association" "my-route-table-association-private-2" {
    subnet_id = aws_subnet.private-subnet-2.id
    route_table_id = aws_route_table.my-private-route-table-2.id
}

/*
Create Security Groups
*/

resource "aws_security_group" "instance-security-group" {
  name = "${var.my_tag}-server-group-public"
  vpc_id  = aws_vpc.my-vpc.id
}

resource "aws_security_group_rule" "allow-myIP-master" {
      security_group_id = aws_security_group.instance-security-group.id
      type              = "ingress"
      from_port         = "0"
      to_port           = "0"
      protocol          = "All"
      cidr_blocks       = var.my_cidr
}
resource "aws_security_group_rule" "allow-http" {
     security_group_id = aws_security_group.instance-security-group.id
     type              = "ingress"
     from_port         = 80
     to_port           = 80
     protocol          = "tcp"
     cidr_blocks       = ["0.0.0.0/0"]
}
resource "aws_security_group_rule" "allow-http-2" {
     security_group_id = aws_security_group.instance-security-group.id
     type              = "ingress"
     from_port         = 8080
     to_port           = 8080
     protocol          = "tcp"
     cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "outbound-master" {
       security_group_id = aws_security_group.instance-security-group.id
       type              = "egress"
       from_port         = 0
       to_port           = 0
       protocol          = -1
       cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group" "allow-other-security-group" {
  name = "${var.my_tag}-allow-other"
  vpc_id  = aws_vpc.my-vpc.id
}

resource "aws_security_group_rule" "allow-ssh-master" {
      security_group_id = aws_security_group.allow-other-security-group.id
      type              = "ingress"
      from_port         = "22"
      to_port           = "22"
      protocol          = "tcp"
      source_security_group_id = aws_security_group.instance-security-group.id
}

resource "aws_security_group_rule" "outbound-master-2" {
       security_group_id = aws_security_group.allow-other-security-group.id
       type              = "egress"
       from_port         = 0
       to_port           = 0
       protocol          = -1
       cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group" "allow-SQL" {
  name = "${var.my_tag}-allow-SQL"
  vpc_id  = aws_vpc.my-vpc.id
}

resource "aws_security_group_rule" "allow-SQL-master" {
      security_group_id = aws_security_group.allow-SQL.id
      type              = "ingress"
      from_port         = "3306"
      to_port           = "3306"
      protocol          = "tcp"
      cidr_blocks       = ["0.0.0.0/0"]

}

resource "aws_security_group_rule" "outbound-SQL" {
       security_group_id = aws_security_group.allow-SQL.id
       type              = "egress"
       from_port         = "3306"
       to_port           = "3306"
       protocol          = "tcp"
       cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "inbound-all-SQL" {
       security_group_id = aws_security_group.allow-SQL.id
       type              = "ingress"
       from_port         = 0
       to_port           = 0
       protocol          = -1
       cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "outbound-all-SQL" {
       security_group_id = aws_security_group.allow-SQL.id
       type              = "egress"
       from_port         = 0
       to_port           = 0
       protocol          = -1
       cidr_blocks       = ["0.0.0.0/0"]
}

/*
Create Jenkins
*/

resource "aws_instance" "jenkins-master" {
  ami               = var.jenkins_master_ami
  key_name          = "Cohort9_shared"
  vpc_security_group_ids   = [aws_security_group.instance-security-group.id]
  subnet_id         = aws_subnet.public-subnet.id
  instance_type     = "t2.micro"
  iam_instance_profile = "ALAcademyJenkinsSuperRole"
  tags = {
    Name = "${var.my_tag}_jenkins_master"
  }
}

resource "aws_instance" "jenkins-agent" {
  ami               = var.jenkins_agent_ami
  key_name          = "Cohort9_shared"
  vpc_security_group_ids = [aws_security_group.instance-security-group.id, aws_security_group.allow-other-security-group.id]
  subnet_id         = aws_subnet.public-subnet.id
  instance_type     = "t2.micro"
  iam_instance_profile = "ALAcademyJenkinsSuperRole"
  tags = {
    Name = "${var.my_tag}_jenkins_agent"
  }
}

resource "aws_route53_record" "www" {
  zone_id = "Z03386713MCJ0LHJCA6DL"
  name    = "${var.my_tag}-jenkins"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.jenkins-master.public_ip]
}

/*
Create Database
*/

resource "aws_db_subnet_group" "gans-subnet" {
    name = "gans-subnet"
    # need to change to private 
    subnet_ids = [aws_subnet.private-subnet.id, aws_subnet.private-subnet-2.id]
}

resource "aws_db_instance" "gansdb" {
  allocated_storage    = 10
  max_allocated_storage = 1000
  engine               = "mariadb"
  db_subnet_group_name = aws_db_subnet_group.gans-subnet.name
  engine_version       = "10.5.13"
  instance_class       = "db.t2.micro"
  availability_zone    = "${var.aws_region}a"
  identifier           = "gansdb" 
  username             = "petclinic"
  password             = "petclinic"
  parameter_group_name = "default.mariadb10.5"
  skip_final_snapshot  = true
  vpc_security_group_ids = [aws_security_group.allow-SQL.id]
  backup_retention_period = 7
  final_snapshot_identifier = "gans-dbfinal-snapshot"
  tags = {
    Schedule = "Dublin-office-hours"
  }
  tags_all = {
    Schedule = "Dublin-office-hours"
  }
}

# output "db_address" {
#   value = aws_db_instance.gansdb.address
# }

data "template_file" "userdatasql" {
  template = "${file("userdatasql.tpl")}"
  vars = {
    hostname = "${aws_db_instance.gansdb.address}"
  }
}

resource "aws_instance" "SQL-master" {
  ami               = var.SQL_ami
  key_name          = "Cohort9_shared"
  vpc_security_group_ids   = [aws_security_group.instance-security-group.id, aws_security_group.allow-other-security-group.id]
  subnet_id         = aws_subnet.public-subnet.id
  instance_type     = "t2.micro"
  tags = {
    Name = "${var.my_tag}_SQL"
  }

  user_data = "${data.template_file.userdatasql.rendered}"
  # user_data = filebase64("${path.module}/userdataSQL.sh")
}

# output "SQL_master_address" {
#   value = aws_instance.public_ip
# }

/*
Create Load balancer and autocaling group
*/

resource "aws_lb" "main-lb" {
    name                        = "${var.my_tag}-lb"
    internal                    = false
    load_balancer_type          = "application"
    security_groups             = [aws_security_group.instance-security-group.id]
    subnets                     = [aws_subnet.public-subnet.id, aws_subnet.public-subnet-2.id]
    tags                        = {
        Name                    = "${var.my_tag}-lb"
    }
}

resource "aws_route53_record" "www2" {
  zone_id = "Z03386713MCJ0LHJCA6DL"
  name    = "bestpetclinic"
  type    = "CNAME"
  ttl     = "300"
  records = [aws_lb.main-lb.dns_name]
}

resource "aws_lb_target_group" "my-tg" {
  name     = "${var.my_tag}-lb-tg"
  port     = 8080
  protocol = "HTTP"
  vpc_id   = aws_vpc.my-vpc.id
}

resource "aws_lb_listener" "tg-listener" {
  load_balancer_arn = aws_lb.main-lb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.my-tg.arn
  }
}

data "template_file" "userdata" {
  template = "${file("userdata.tpl")}"
  vars = {
    hostname = "${aws_db_instance.gansdb.address}"
  }
}

resource "aws_launch_template" "my-template" {
  name          = "${var.my_tag}-template"
  # need to edit for our image
  image_id      = var.petclinic_ami
  instance_type = "t2.micro"
  vpc_security_group_ids = [aws_security_group.instance-security-group.id, aws_security_group.allow-other-security-group.id]
  key_name = "Cohort9_shared"
  tag_specifications {
    resource_type = "instance"
    tags = {
        Name = "${var.my_tag}-petclinic-instance"
    }
  }

  user_data = "${base64encode(data.template_file.userdata.rendered)}"

}

resource "aws_autoscaling_group" "my-asg" {
  desired_capacity   = 2
  max_size           = 6
  min_size           = 2
  vpc_zone_identifier = [aws_subnet.public-subnet.id, aws_subnet.public-subnet-2.id]
  launch_template {
    id      = aws_launch_template.my-template.id
    version = "$Latest"
  }
  target_group_arns = [aws_lb_target_group.my-tg.arn]
}