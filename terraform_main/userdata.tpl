#! /bin/bash

sudo sh -c 'cat > /home/ubuntu/spring-petclinic/application.properties <<_END_

 database=petclinic
 spring.datasource.url=jdbc:mysql://${hostname}:3306/petclinic
 spring.datasource.username=petclinic
 spring.datasource.password=petclinic


'

sleep 10 

bash /home/ubuntu/petclinic_launch.sh start

sleep 10