#! /bin/bash

hostname=$(terraform output -raw db_address)

sudo sh -c 'cat > /etc/environment <<_END_

 database=petclinic
 spring.datasource.url=jdbc:mysql://$hostname:3306/petclinic
 spring.datasource.username=petclinic
 spring.datasource.password=petclinic
 
 _END_

'

bash petclinic.sh start