#! /bin/bash

sleep 180

# hostname="gansdb.c6jtmnlypkzc.eu-west-1.rds.amazonaws.com"
hostname =$(terraform output -raw db_address)
pass="petclinic"

mysql -h $hostname -P 3306 -u petclinic -p$pass < /home/ec2-user/spring-petclinic/src/main/resources/db/mysql/user.sql
mysql -h $hostname -P 3306 -u petclinic -p$pass petclinic < /home/ec2-user/spring-petclinic/src/main/resources/db/mysql/schema.sql
mysql -h $hostname -P 3306 -u petclinic -p$pass petclinic < /home/ec2-user/spring-petclinic/src/main/resources/db/mysql/data.sql

