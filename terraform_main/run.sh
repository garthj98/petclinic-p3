IP=$1
KEY=$2

# Copy this folder to teh EC2 instance 
scp -i $KEY -r terraform_main ec2-user@$IP:~/

ssh -o StrictHostKeyChecking=no -i $KEY ec2-user@$IP << EOF
terraform -chdir='terraform_main' init
terraform -chdir='terraform_main' apply -auto-approve -var='petclinic_ami='$(cat terraform_main/petclinicami.txt) 
EOF



# cat ./petclinic_launch/amibuilder.txt | grep 'eu-west-1:' | awk '{print $2}' > ./terraform_main/petclinicami.txt