# Terraform 

## Pre-requisits 

- An ec2 instances on AWS with assuming the IAM role Jenkins super user you can access with terraform downoaded on it
- Edit the `variables.tf` file varliable `variable "my_cidr"` with the IP adresses of the machines you would like to have access to port 22 of the machines you are creating 
- be in the petclinic-p3 directory

```bash
# Clone repository to your local machine
$ git clone git@bitbucket.org:garthj98/petclinic-p3.git

# Make a change to the petclinic branch and then push to petclinic branch
$ run.sh $1 $2
## Where `$1` is the public IP of the AWS EC2 instance described above and `$2` is the ssh key location used to create this instance 
```

The terraform state is stored in a bucket on AWS to allow the terraform script to be able to be used from either Jenkins or other machines.

This will create VPC, subnets, security groups, Jenkins machines, RDS, loadbalancer, autoscalling gorup and link all of these. Look at Jenkins folder for how to link the master and agent nodes.

## Infra-structure going down 

If any infrastructure goes does you can log into theh terraform machnice using 
- `ssh -i $1 ec2-user@$2`
- navigate into the file `~/petclinic-p3/terraform_main`
- Where `$2` is the public IP of the AWS EC2 instance described above and `$1` is the ssh key location used to create this instance
- The you can target the infrastucture that has gone down to destroy, `terraform destroy target='infrastucture_gone.down'`
- Then recreate it using `terraform -chdir='terraform_main' apply -auto-approve -var='petclinic_ami='$(cat petclinicami.txt)`


### Who to contact

Discuss any issues, questions, or just have some feedback on our code, you can contact **Garth** at: garth@automationlogic.com 

Our working hours are Monday to Friday 9am - 5:30pm