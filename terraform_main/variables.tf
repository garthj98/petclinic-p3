variable "my_tag" {
	description = "Name to tag all elements with"
	default = "GANS"
}

variable "aws_region" {
    description = "AWS region"
    default = "eu-west-1"
}

variable "vpc_cidr" {
    description = "CIDR for the whole VPC"
    default = "10.0.0.0/16"
}

variable "public_subnet_cidr" {
    description = "CIDR for the public subnet"
    default = "10.0.1.0/24"    
}

variable "public_subnet_cidr_2" {
    description = "CIDR for the public subnet 2"
    default = "10.0.2.0/24"    
}

variable "private_subnet_cidr" {
    description = "CIDR for the private subnet "
    default = "10.0.3.0/24"    
}

variable "private_subnet_cidr_2" {
    description = "CIDR for the private subnet "
    default = "10.0.4.0/24"    
}

variable "my_cidr" {
    description = "My IPv4"
    default = ["86.11.200.116/32", "82.13.193.166/32", "2.123.248.210/32", "82.18.249.32/32"]
}

variable "jenkins_master_ami" {
    description = "Jenkins master ami"
    default = "ami-0fd352ce5a8aaece5"
}

variable "jenkins_agent_ami" {
    description = "Jenkins agent ami"
    default = "ami-011e4185285f941f5"
}

variable "petclinic_ami" {
    description = "Petclinic"
    # default = "ami-0420308ee42c3bdea"
    # default = "ami-05fb969fe81960390"
}

variable "SQL_ami" {
    description = "SQL"
    default = "ami-085d1014317a83c72"
}