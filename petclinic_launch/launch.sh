#installs maven
sudo apt update 
sudo apt install maven -y

#installs jdk
sudo apt update -y
sudo apt install default-jdk -y

#clones petclinic repo and runs it
git clone https://github.com/spring-projects/spring-petclinic.git

touch petclinic_launch.sh

sudo sh -c """"
cat > petclinic_launch.sh <<_END_
#!/bin/bash
# chkconfig: 345 90 90
# description: Manage PetClinic

case \$1 in
  start)  # Start Vault
          cd /home/ubuntu/spring-petclinic
          touch petclinic.pid
          nohup ./mvnw spring-boot:run &
          sleep 10
          ps -ef | grep 'spring-petclinic' | awk '{print \$2}' > /home/ubuntu/spring-petclinic/petclinic.pid
          ;;
  stop)   # Kill you script here
	  if [ -e /home/ubuntu/spring-petclinic/petclinic.pid ]
	  then
          	kill \$(cat /home/ubuntu/spring-petclinic/petclinic.pid)
	  fi
          ;;

  status) # Show something useful
          ps -ef | grep -v grep | grep 'spring-petclinic' >/dev/null 2>/dev/null
	  if (( \$? == 0 ))
	  then
		echo "PetClinic is running"
	  else
		echo "PetClinic is not running"
	  fi
          ;;
  *) # Some default message
     echo "Use either start, stop or status"
     ;;
esac
_END_
""""

cd spring-petclinic
touch application.properties

 sudo sh -c "cat > application.properties <<_END_
 database=petclinic
 spring.datasource.url=jdbc:mysql://afks-petclinic-server.c6jtmnlypkzc.eu-west-1.rds.amazonaws.com:3306/petclinic
 spring.datasource.username=admin
 spring.datasource.password=arslan00
"


# bash petclinic.sh start
#mvn -Dmaven.test.skip=true package
