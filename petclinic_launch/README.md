# How to launch petclinic

## Installing files

To install all prequisites, create a new instance and run `bash launch.sh`
This creates all the folders and files needed to launch petlcinic.

## Launch petclinic

To launch petclinic, you can use the init script and use the command `bash petclinic_launch.sh start` which starts an instance of petclinic in the background.
Other commands include `bash petclinic_launch stop` which will kill any petclinic processes in the background, `bash petclinic_launch status` which allows user to check if any petclinic processes are running in the background.

init commands will help for automation in scripts.

for any clarification please contact: arslan@automationlogic.com

final test 2?