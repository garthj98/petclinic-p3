packer {
  required_plugins {
    amazon = {
      version = ">= 0.0.2"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

source "amazon-ebs" "base_values" {
  # assume_role {
  #   role_arn     = "arn:aws:iam::242392447408:role/OrganizationAccountAccessRole"
  #   }
  ami_name          = "gans-petclinic_ami-v1"
  instance_type     = "t2.micro"
  region            = "eu-west-1"
  source_ami        = "ami-08ca3fed11864d6bb"
  vpc_id            = "vpc-4bb64132"
  ssh_username      = "ubuntu"
}

build {
  sources = ["source.amazon-ebs.base_values"]
  name = "gans-petclinic-ami-v1"
  provisioner "shell" {
    script = "./petclinic_launch/launch.sh"
  } 
}