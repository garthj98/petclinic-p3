
provider "aws" {
    shared_credentials_files =  ["/Users/nicoleghessen/Downloads/credentials"]
    profile = "Academy"
    region = "eu-west-1"
}

# data "aws_vpc" "our_vpc" {
#     id = "vpc-013595a0c9deb4297"
# }



resource "aws_db_subnet_group" "gans-subnet" {
    name = "gans-subnet"
    subnet_ids = ["subnet-04d883a1694f3cad3", "subnet-02bf8e639e5c92ea0"]
}

# data "aws_subnet_ids" "subnet_ids" {
#     vpc_id = data.aws_vpc.our_vpc.id
#     tags = {
#         Name = "gans-vpc1"
#     }
# }

resource "aws_db_instance" "gansdb" {
  allocated_storage    = 10
  max_allocated_storage = 1000
  engine               = "mariadb"
  db_subnet_group_name = aws_db_subnet_group.gans-subnet.name
  engine_version       = "10.5.13"
  instance_class       = "db.t2.micro"
  availability_zone    = "eu-west-1a"
  db_name              = "gansdb" 
  identifier           = "gansdb" 
  username             = "petclinic"
  password             = "petclinic"
  parameter_group_name = "default.mariadb10.5"
  skip_final_snapshot  = false
  final_snapshot_identifier = "gansdb-snapshot-final"
  
}
