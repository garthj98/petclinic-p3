# provider "aws" {
#     access_key = var.aws_access_key
#     secret_key = var.aws_secret_key
#     region = "eu-west-1"
# }

# data "aws_vpc" "our_vpc" {
#     id = "vpc-09f9ff1fdfd21651b"
# }

# data "aws_subnet_ids" "subnet_ids" {
#     vpc_id = data.aws_vpc.our_vpc.id
#     tags = {
#         Name = "private-*"
#     }
# }

# resource "aws_db_subnet_group" "prod_mariadb" {
#     name = "prod-mariadb"
#     subnet_ids = data.aws_subnet_ids.subnet_ids.ids
# }

# resource "aws_db_instance" "the_db" {
#     engine = "mariadb"
#     engine_version = "10.5"
#     instance_class = "db.t2.micro"
#     name = "prodmariadb"
#     identifier = "prod-mariadb"
#     username = "root"
#     password = "<some-secure-password>"
#     parameter_group_name = "default.mariadb10.5"
#     db_subnet_group_name = aws_db_subnet_group.prod_mariadb.name
#     vpc_security_group_ids = [aws_security_group.allow_mariadb.id]
#     skip_final_snapshot = true
#     allocated_storage = 50
#     max_allocated_storage = 1000
# }

# resource "aws_security_group" "allow_mariadb" {
#     name = "prod-mariadb-rds-sg"
#     description = "Allow TLS inbound traffic"
#     vpc_id = data.aws_vpc.our_vpc.id

#     ingress {
#         description = "Mariadb Access from within the VPC"
#         from_port = 3306
#         to_port = 3306
#         protocol = "tcp"
#         cidr_blocks = ["10.0.0.0/8"]
#     }

#     egress {
#         from_port = 0
#         to_port = 0
#         protocol = "-1"
#         cidr_blocks = ["0.0.0.0/0"]
#     }

#     tags = {
#         Name = "Allow access to prod mariadb rds"
#     }
# }

# output "this_db_name" {
#     value = aws_db_instance.the_db.name
# }

# output "this_db_instance_address" {
#     value = aws_db_instance.the_db.address
# }

# output "this_db_instance_arn" {
#     value = aws_db_instance.the_db.arn
# }

# output "this_db_instance_domain" {
#     value = aws_db_instance.the_db.domain
# }

# output "this_db_instance_endpoint" {
#     value = aws_db_instance.the_db.endpoint
# }

# output "this_db_instance_status" {
#     value = aws_db_instance.the_db.status
# }