## Amazon RDS (Amazon Relational Database Service)

- Before you can get value from your data, you need to store it efficently. This is why we have chosen RDS to work with your database. It reduces your storage cost and minimises effort in managing the database.

- Amazon RDS manages the backups, automatic failure detections, and recovery of your data which is important for the petclinic business.

- The RDS database will be created when you run the main terraform script provided in the Terraform main directory.

Any changes you need to make to the RDS can be done through the AWS console.

If you have any issues or problems please feel free to contact me via email: nicole@automationlogic.com

